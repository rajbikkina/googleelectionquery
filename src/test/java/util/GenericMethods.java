package util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GenericMethods {

	public static Boolean checkDuplicates(List<Long> listOfValues) {
		Boolean dupFound = false;
		Set<Long> set = new HashSet<Long>(listOfValues);
		if (listOfValues.size() != set.size()) {
			dupFound = true;
		}
		return dupFound;
	}
}
