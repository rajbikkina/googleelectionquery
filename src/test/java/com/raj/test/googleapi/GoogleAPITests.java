package com.raj.test.googleapi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.raj.test.googleapi.beans.Elections;

import io.restassured.response.Response;
import util.GenericMethods;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import static io.restassured.RestAssured.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

@TestMethodOrder(OrderAnnotation.class)
public class GoogleAPITests extends BaseTest {

	private static final Logger logger = LogManager.getLogger(GoogleAPITests.class.getName());
	private static Response response;
	private static Elections listOfElections;

	@BeforeAll
	public static void beforeAll() {
		logger.info("Begin ElectionQuery Test Suite");
		response = get(googleapiBasePath + "/" + googleapiVersion + "/elections?key=" + googleapiKey);
		listOfElections = response.body().as(Elections.class);
	}

	@BeforeEach
	public void beforeEach() {
		//logger.info("Begin Test");
		// response = get(googleapiBasePath + "/" + googleapiVersion + "/elections?key="+ googleapiKey);
		// listOfElections = response.body().as(Elections.class);
	}

	/*
	 * This is a valid test, validates for successful STATUS code
	 */
	@Test
	@Order(1)
	public void electionValidTest() {
		logger.info("Begin electionValidTest");
		Assertions.assertEquals(200, response.getStatusCode(),"Invalid response status code");
		logger.info("electionValidTest is success with response code " + response.getStatusCode());
		logger.info("End electionValidTest");
	}

	/*
	 * This test verifies electionQuery for invalid API key
	 */
	@Test
	@Order(2)
	public void electionInvalidKey() {
		logger.info("Begin electionInvalidKey Test");
		Response res = get(googleapiBasePath + "/" + googleapiVersion + "/elections?key=" + invalidGoogleapiKey);
		Assertions.assertEquals(400, res.getStatusCode());
		logger.info("electionInvalidKey response code matches " + res.getStatusCode());
		logger.info("End electionInvalidKey Test");

	}

	/*
	 * This test verifies electionQuery for invalid google api version
	 */
	@Test
	@Order(2)
	public void electionInvalidversion() {
		logger.info("Begin electionInvalidversion Test");
		Response res = get(googleapiBasePath + "/" + inValidGoogleapiVersion + "/elections?key=" + googleapiKey);
		Assertions.assertEquals(404, res.getStatusCode());
		logger.info("electionInvalidversion response code matches " + res.getStatusCode());
		logger.info("End electionInvalidversion Test");

	}

	/**
	 * This test validates for getOcdDivisionId is of type general election or state
	 * election
	 */
	@Test
	@Order(3)
	public void election() {
		logger.info("Begin List of election Test");
		Elections listOfElections = response.body().as(Elections.class);

		for (int i = 0; i < listOfElections.elections.size(); i++) {
			Boolean isStateElection = false;
			Boolean isNotGeneralOrStateElection = false;
			String[] eleArray = null;

			try {
				eleArray = listOfElections.elections.get(i).getOcdDivisionId().split("/");
				Assertions.assertTrue(eleArray[0].equals("ocd-division"));
				Assertions.assertTrue(eleArray[1].equals("country:us"));
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("check array length");
			}

			if (eleArray.length > 2) {
				if (eleArray[2].contains("state")) {
					isStateElection = true;
				} else {
					isNotGeneralOrStateElection = true;
				}
			}

			if (isStateElection == true) {
				logger.info(listOfElections.elections.get(i).getOcdDivisionId() + " is STATE election");
			} else if (isNotGeneralOrStateElection == true) {
				logger.info(
						listOfElections.elections.get(i).getOcdDivisionId() + " is NOT STATE or GENERAL election");
			} else {
				logger.info(listOfElections.elections.get(i).getOcdDivisionId() + " is GENERAL election");
			}
		}
		logger.info("list of Elections verified");
		logger.info("End List of election Test");
	}

	/**
	 * This test verifies if electionQuery response contains duplicate id's
	 */
	@Test
	@Order(3)
	public void electionIdTest() {
		logger.info("Begin electionIdTest");
		List<Long> idList = new ArrayList<Long>();

		for (int i = 0; i < listOfElections.elections.size(); i++) {
			idList.add(listOfElections.elections.get(i).getId());
		}

		Boolean val = GenericMethods.checkDuplicates(idList);
		if (val == true) {
			Assertions.fail();
			logger.info("Duplicate ElectionId's found");

		}
		logger.info("ElectionId's are unique");
		logger.info("End electionIdTest");
	}

	/*
	 * Validates if Day of the election is in YYYY-MM-DD format
	 */
	@Test
	@Order(3)
	public void electionDateFormatTest() {
		logger.info("Begin electionDateFormatTest");
		for (int i = 0; i < listOfElections.elections.size(); i++) {
			Matcher matcher = pattern.matcher(listOfElections.elections.get(i).getElectionDay());
			Assertions.assertTrue(matcher.matches());
		}
		logger.info("All Election Dates are in YYYY-MM-DD Format");
		logger.info("End electionDateFormatTest");
	}

	@AfterEach
	public void afterEach() {
		//logger.info("End Test");
	}

	@AfterAll
	public static void afterAll() {
		logger.info("End ElectionQuery Test Suite");
	}

}
