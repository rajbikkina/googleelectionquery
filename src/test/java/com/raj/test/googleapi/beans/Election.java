package com.raj.test.googleapi.beans;

public class Election {
	public Long id;
	public String name;
	public String electionDay;
	public String ocdDivisionId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getElectionDay() {
		return electionDay;
	}
	public void setElectionDay(String electionDay) {
		this.electionDay = electionDay;
	}
	public String getOcdDivisionId() {
		return ocdDivisionId;
	}
	public void setOcdDivisionId(String ocdDivisionId) {
		this.ocdDivisionId = ocdDivisionId;
	}
	
	@Override
	public String toString() {
		return "Election [id=" + id + ", name=" + name + ", electionDay=" + electionDay + ", ocdDivisionId="
				+ ocdDivisionId + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((electionDay == null) ? 0 : electionDay.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ocdDivisionId == null) ? 0 : ocdDivisionId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Election other = (Election) obj;
		if (electionDay == null) {
			if (other.electionDay != null)
				return false;
		} else if (!electionDay.equals(other.electionDay))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ocdDivisionId == null) {
			if (other.ocdDivisionId != null)
				return false;
		} else if (!ocdDivisionId.equals(other.ocdDivisionId))
			return false;
		return true;
	}
}
