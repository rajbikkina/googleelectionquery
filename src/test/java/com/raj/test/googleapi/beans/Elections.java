package com.raj.test.googleapi.beans;

import java.util.List;

public class Elections {
	public String kind;
	public List<Election> elections;
	
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public List<Election> getElections() {
		return elections;
	}
	public void setElections(List<Election> elections) {
		this.elections = elections;
	}
	
	@Override
	public String toString() {
		return "Elections [kind=" + kind + ", elections=" + elections + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elections == null) ? 0 : elections.hashCode());
		result = prime * result + ((kind == null) ? 0 : kind.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Elections other = (Elections) obj;
		if (elections == null) {
			if (other.elections != null)
				return false;
		} else if (!elections.equals(other.elections))
			return false;
		if (kind == null) {
			if (other.kind != null)
				return false;
		} else if (!kind.equals(other.kind))
			return false;
		return true;
	}
}
